# Software considered harmful

In the sidebar on the left, you can find why i consider some points of
the software bad.

I did not fall for the minimalism meme, but sometimes Minimalistic
software less bad than non minimalistic software. Sometimes a
"bloated" program is way better than non bloated ones, for example,
Emacs vs Vim. Or Firefox vs Surf.



Here you can find a table (Consider this as a TODO) I am not including
GNU utilities because, altough they don't follow standards, their
extensions are kinda useful. So I don't consider them harmful.

<table>
  <tr>
    <th>Harmful things</th>
    <th>Better alternatives</th>
  </tr>
  <tr>
    <td>KDE, Gnome</td>
    <td>i3wm, dwm, Xfce</td>
  </tr>
  <tr>
    <td>Windows, MacOS</td>
    <td>GNU/Linux, BSD, Haiku</td>
  </tr>
  <tr>
    <td>XML</td>
    <td>JSON, CVS</td>
  </tr>
  <tr>
    <td>Vim, Vi</td>
    <td>nano, Emacs, ed, joe, zile, jed</td>
  </tr>
  <tr>
    <td>MacBooks</td>
    <td>ThinkPads, Toughbooks</td>
  </tr>
  <tr>
    <td>Java, C++, Python, Clojure</td>
    <td>Perl, C, Common Lisp, Scheme</td>
  </tr>
  <tr>
    <td>Clang</td>
    <td>GCC, TCC</td>
  </tr>
  <tr>
    <td>Wayland</td>
    <td>X11</td>
  </tr>
  <tr>
    <td>GTK, QT</td>
    <td>Tk, curses</td>
  </tr>
  <tr>
    <td>Intel, Realtek, Broadcom network drivers</td>
    <td>Atheros (ath9k/ath5k compatible)</td>
  </tr>
  <tr>
    <td>Object oriented programming</td>
    <td>Procedural, or even better, functional programming</td>
  </tr>
  <tr>
    <td>Electron</td>
    <td>Throwing yourself to an active volcano, you cannot be saved</td>
  </tr>
  <tr>
    <td>Wordpress</td>
    <td>[werc](https://werc.cat-v.org), [cleg](https://git.qorg11.net/cleg)</td>
  </tr>
  <tr>
    <td>Chromium, G\*\*gle Chrome, Safari, M\*cr\*soft Edge, Brave, Opera, Opera Gaming, Vivaldi</td>
    <td>Firefox, Librewolf, Qutebrowser, Palemoon, GNU IceCat, Goddamit, Even surf is better than those browsers</td>
  </tr>
  <tr>
    <td>Google Search, Bing</td>
    <td>DuckDuckGo, Qwant and the best of all, Searx</td>
  </tr>
  <tr>
    <td>WhatsApp, Messenger, Twitter DM's, Discord</td>
    <td>IRC, xmpp, Matrix, Mumble</td>
  </tr>
  <tr>
    <td>Gmail, Hotmail, Yahoo Mail</td>
    <td>Self hosted email server, [Riseup](https://riseup.net)</td>
  </tr>
  <tr>
    <td>s̜̣͕͠y̹̼̮̱̞͇͢s̟̺̮̼̳ṱe̴̞̜͙m̱͜d̼</td>
    <td>runit, OpenRC, SysVinit</td>
  </tr>
  <tr>
    <td>MIT, BSD</td>
    <td>GPL, [VPL](https://viralpubliclicense.org/)</td>
</table>
